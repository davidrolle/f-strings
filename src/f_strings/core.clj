(ns f-strings.core
  (:require [clojure.string :as str]))

(def ^:dynamic *escape-char* \^)

(defn- tokenize-f-str
  "Separates an f-string into its parts (plaintext and forms)"
  [f-str]
  (loop [s f-str
         escaped? false
         depth 0    ;; number of currently open parens
         expr nil   ;; current text or form
         tokens []] ;; for storing the return values
    (let [c (first s)
          r (rest s)
          expr+c (str expr c)
          tokens+expr (if expr (conj tokens expr) tokens)]
      (if (nil? c)
        tokens+expr
        (if escaped?
          (recur r false depth expr+c tokens)
          (condp = c
            *escape-char* (recur r true depth expr tokens)
            \( (if (= depth 0)
                 (recur r false 1 c tokens+expr)
                 (recur r false (inc depth) expr+c tokens))
            \) (if (= depth 1)
                 (recur r false 0 nil (conj tokens (read-string expr+c)))
                 (recur r false (dec depth) expr+c tokens))
            (recur r false depth expr+c tokens)))))))

(defn- expand-kws
  "Pre-process an f-string by injecting map lookups for all keywords"
  [f-str m]
  (str/replace
    f-str
    #"\[(?:(?<!\^)::?[\w\.\-_?+*/\!]+\s*)+\]|(?<!\^)::?[\w\.\-_?+*/\!]+"
    (fn [kws]
      (if (= (first kws) \[)
        (str "(get-in " m " " kws ")")
        (str "(" kws " " m ")")))))

(defmacro f
  "Takes an f-string and returns the output string

  Similar to python, an f-string is a string literal which contains forms that
  should be evaluated and their results placed within the output string. In
  Python, these are surrounded with {}, but, since this is Clojure, we will
  use parens ():

  `(f \"1 + 1 is (+ 1 1)!\")` => `\"1 + 1 is 2!\"`

  When passed a map as a second argument, interprets keywords as map lookups:

  `(f \"x = :x\" {:x 1})` => `\"x = 1\"`

  Vectors of keywords can also be passed to look up nested values:

  `(f \"[:a :b]\" {:a {:b 1} :b {:a 2}})` => `\"1\""
  ([f-str]
   (cons 'str (tokenize-f-str f-str)))
  ([f-str m]
   (list 'f (expand-kws f-str m))))


;; TODO: Add formatting specifiers e.g. ":.2f"

